package gcd;

import gcd.programs.bad.*;
import gcd.tests.*;
import gcd.tests.suites.*;

import java.util.*;

import wv.TestAllVariations;

public class TestAllVariationsImpl {
	
	

	public static void main(String args[])
	{
		List<Class<?>> classes;
		List<Class<?>> programClasses;
		classes = new ArrayList<Class<?>>();
		classes.add(Gcd_TestSuite_1_2_3_4_5.class);
		classes.add(Gcd_TestSuite_1_2_3_4_6.class);
		classes.add(Gcd_TestSuite_1_2_3_4.class);
		classes.add(Gcd_TestSuite_1_2_3_5_6.class);
		classes.add(Gcd_TestSuite_1_2_3_5.class);
		classes.add(Gcd_TestSuite_1_2_3_6.class);
		classes.add(Gcd_TestSuite_1_2_3.class);
		classes.add(Gcd_TestSuite_1_2_4_5_6.class);
		classes.add(Gcd_TestSuite_1_2_4_5.class);
		classes.add(Gcd_TestSuite_1_2_4_6.class);
		classes.add(Gcd_TestSuite_1_2_4.class);
		classes.add(Gcd_TestSuite_1_2_5_6.class);
		classes.add(Gcd_TestSuite_1_2_5.class);
		classes.add(Gcd_TestSuite_1_2_6.class);
		classes.add(Gcd_TestSuite_1_2.class);
		classes.add(Gcd_TestSuite_1_3_4_5_6.class);
		classes.add(Gcd_TestSuite_1_3_4_5.class);
		classes.add(Gcd_TestSuite_1_3_4_6.class);
		classes.add(Gcd_TestSuite_1_3_4.class);
		classes.add(Gcd_TestSuite_1_3_5_6.class);
		classes.add(Gcd_TestSuite_1_3_5.class);
		classes.add(Gcd_TestSuite_1_3_6.class);
		classes.add(Gcd_TestSuite_1_3.class);
		classes.add(Gcd_TestSuite_1_4_5_6.class);
		classes.add(Gcd_TestSuite_1_4_5.class);
		classes.add(Gcd_TestSuite_1_4_6.class);
		classes.add(Gcd_TestSuite_1_4.class);
		classes.add(Gcd_TestSuite_1_5_6.class);
		classes.add(Gcd_TestSuite_1_5.class);
		classes.add(Gcd_TestSuite_1_6.class);
		classes.add(Gcd_TestSuite_2_3_4_5_6.class);
		classes.add(Gcd_TestSuite_2_3_4_5.class);
		classes.add(Gcd_TestSuite_2_3_4_6.class);
		classes.add(Gcd_TestSuite_2_3_4.class);
		classes.add(Gcd_TestSuite_2_3_5_6.class);
		classes.add(Gcd_TestSuite_2_3_5.class);
		classes.add(Gcd_TestSuite_2_3_6.class);
		classes.add(Gcd_TestSuite_2_3.class);
		classes.add(Gcd_TestSuite_2_4_5_6.class);
		classes.add(Gcd_TestSuite_2_4_5.class);
		classes.add(Gcd_TestSuite_2_4_6.class);
		classes.add(Gcd_TestSuite_2_4.class);
		classes.add(Gcd_TestSuite_2_5_6.class);
		classes.add(Gcd_TestSuite_2_5.class);
		classes.add(Gcd_TestSuite_2_6.class);
		classes.add(Gcd_TestSuite_3_4_5_6.class);
		classes.add(Gcd_TestSuite_3_4_5.class);
		classes.add(Gcd_TestSuite_3_4_6.class);
		classes.add(Gcd_TestSuite_3_4.class);
		classes.add(Gcd_TestSuite_3_5_6.class);
		classes.add(Gcd_TestSuite_3_5.class);
		classes.add(Gcd_TestSuite_3_6.class);
		classes.add(Gcd_TestSuite_4_5_6.class);
		classes.add(Gcd_TestSuite_4_5.class);
		classes.add(Gcd_TestSuite_4_6.class);
		classes.add(Gcd_TestSuite_5_6.class);
		classes.add(Gcd_TestSuite_All.class);
		classes.add(Gcd_1_Test.class);
		classes.add(Gcd_2_Test.class);
		classes.add(Gcd_3_Test.class);
		classes.add(Gcd_4_Test.class);
		classes.add(Gcd_5_Test.class);
		classes.add(Gcd_6_Test.class);
		
		programClasses = new ArrayList<Class<?>>();
		programClasses.add(Gcd_Breaking_1_2_3_4_5.class);
		programClasses.add(Gcd_Breaking_1_2_3_4_6.class);
		programClasses.add(Gcd_Breaking_1_2_3_5_6.class);
		programClasses.add(Gcd_Breaking_1_2_3_6.class);
		programClasses.add(Gcd_Breaking_1_2_4_5.class);
		
		programClasses.add(Gcd_Breaking_1_2_5_6.class);
		programClasses.add(Gcd_Breaking_1_2_5.class);
		programClasses.add(Gcd_Breaking_1_2_a.class);
		programClasses.add(Gcd_Breaking_1_2_b.class);
		programClasses.add(Gcd_Breaking_1_3_4.class);
		
		programClasses.add(Gcd_Breaking_1_5_6.class);
		programClasses.add(Gcd_Breaking_1_5.class);
		programClasses.add(Gcd_Breaking_2_3_4_5_6.class);
		programClasses.add(Gcd_Breaking_2_3_4_6.class);
		programClasses.add(Gcd_Breaking_2_3_4_a.class);
		
		programClasses.add(Gcd_Breaking_2_3_4_b.class);		
		programClasses.add(Gcd_Breaking_2_3_5_6_a.class);
		programClasses.add(Gcd_Breaking_2_3_5_6_b.class);
		programClasses.add(Gcd_Breaking_2_3_a.class);
		programClasses.add(Gcd_Breaking_2_3_b.class);
		
		programClasses.add(Gcd_Breaking_2_4_5.class);
		programClasses.add(Gcd_Breaking_2_4.class);		
		programClasses.add(Gcd_Breaking_2_5_6.class);
		programClasses.add(Gcd_Breaking_2_5.class);
		programClasses.add(Gcd_Breaking_2_6.class);
		
		programClasses.add(Gcd_Breaking_2.class);
		programClasses.add(Gcd_Breaking_3_4_6.class);
		programClasses.add(Gcd_Breaking_3_4.class);		
		programClasses.add(Gcd_Breaking_5_6.class);
		programClasses.add(Gcd_Breaking_5.class);
		
		TestAllVariations.testAllVariations(new GcdTestUtility(), classes, programClasses);
		
	}

}
