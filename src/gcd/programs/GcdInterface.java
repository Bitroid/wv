package gcd.programs;

/**
 * An interface for computing the gcd.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public interface GcdInterface {
	
	/**
	 * Computing the gcd of the two given numbers.
	 */
	public int gcdIterative(int x, int y);
}
