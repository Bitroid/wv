package gcd.programs.bad;

import gcd.programs.GcdInterface;

/**
 * Breaking the following properties of gcd:
 * 		2. Commutative
 * 		4. y mod gcd == 0
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class Gcd_Breaking_2_4 implements GcdInterface{
		
	/**
	 * Computing the gcd of the two given numbers while breaking the properties 2 and 4.
	 */
	@Override
	public int gcdIterative(int x, int y){
		return x;
	}
}
