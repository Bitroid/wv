package gcd.programs.bad;

import gcd.programs.GcdInterface;

/**
 * First way of breaking the following properties of gcd:
 * 		2. Commutative
 * 		3. x mod gcd == 0
 * 		 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class Gcd_Breaking_2_3_a implements GcdInterface{
	
	/**
	 * Computing the gcd of the two given numbers while breaking the properties 2 and 3.
	 */
	@Override
	public int gcdIterative(int x, int y){
		return y;
	}
}
