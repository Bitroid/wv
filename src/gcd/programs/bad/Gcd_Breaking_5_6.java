package gcd.programs.bad;

import gcd.programs.GcdInterface;

/**
 * Breaking the following properties of gcd:
 * 		5. Biggest
 * 		6. Multiplicative
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class Gcd_Breaking_5_6 implements GcdInterface{
	
	/**
	 * Computing the gcd of the two given numbers while breaking the properties 5 and 6.
	 */
	@Override
	public int gcdIterative(int x, int y){
		if (x % 2 == 0 && y % 2 == 0) {
			return 2;
		}
		return 1;
	}
}
