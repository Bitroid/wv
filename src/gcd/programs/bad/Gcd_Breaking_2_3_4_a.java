package gcd.programs.bad;

import gcd.programs.GcdInterface;

/**
 * First way of breaking the following properties of gcd:
 * 		2. Commutative
 * 		3. x mod gcd == 0
 * 		4. y mod gcd == 0
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class Gcd_Breaking_2_3_4_a implements GcdInterface{
	
	/**
	 * Computing the gcd of the two given numbers while breaking the properties 2, 3 and 4.
	 */
	@Override
	public int gcdIterative(int x, int y){
		int k = 1;
		int gcd = 1;
		while ((k <= x) && (k <= y)) {
		    if ((x % k == 0)) {
		        gcd = k;
		    }
		    k++;
		}
		return gcd;
	}
}
