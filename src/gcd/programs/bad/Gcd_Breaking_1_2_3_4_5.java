package gcd.programs.bad;

import gcd.programs.GcdInterface;

/**
 * Breaking the following properties of gcd:
 * 		1. Associative
 * 		2. Commutative
 * 		3. x mod gcd == 0
 * 		4. y mod gcd == 0
 * 		5. Biggest
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class Gcd_Breaking_1_2_3_4_5 implements GcdInterface{

	/**
	 * Computing the gcd of the two given numbers while breaking the properties 1, 2, 3, 4 and 5.
	 */
	@Override
	public int gcdIterative(int x, int y){
		if (x > y){
			return 1;
		}
		return 0;
	}

}
