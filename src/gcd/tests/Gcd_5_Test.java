package gcd.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * A testclass containing a test for the fifth property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class Gcd_5_Test {

	/**
	 * Testing the fifth property: biggest.
	 */
	@Test
	public void testGrootste(){
		assertTrue(GcdTestUtility.getObjectOfClassToTest().gcdIterative(1052, 52) > 2);
		assertTrue(GcdTestUtility.getObjectOfClassToTest().gcdIterative(36, 12) > 3);
	}
	
}
