package gcd.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * A testclass containing a test for the fourth property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class Gcd_4_Test {
	
	/**
	 * Testing the fourth property: y mod gcd == 0.
	 */
	@Test
	public void testDeeltQ(){
		assertTrue((52 % GcdTestUtility.getObjectOfClassToTest().gcdIterative(1052,  52) == 0));		
	}
	

}
