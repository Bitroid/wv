package gcd.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * A testclass containing a test for the second property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class Gcd_2_Test {
	
	/**
	 * Testing the second property: commutative.
	 */
	@Test
	public void testCommutatief(){
		assertTrue(GcdTestUtility.getObjectOfClassToTest().gcdIterative(1052, 52) == GcdTestUtility.getObjectOfClassToTest().gcdIterative(52, 1052));
	}
}
