package gcd.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gcd.tests.Gcd_4_Test;
import gcd.tests.Gcd_6_Test;

/**
 * A testsuite for the following properties:
 * 		4. y mod gcd == 0
 * 		6. Multiplicative
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Gcd_4_Test.class, Gcd_6_Test.class })
public class Gcd_TestSuite_4_6 {

}
