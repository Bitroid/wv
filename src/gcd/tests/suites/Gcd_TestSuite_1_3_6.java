package gcd.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gcd.tests.Gcd_1_Test;
import gcd.tests.Gcd_3_Test;
import gcd.tests.Gcd_6_Test;

/**
 * A testsuite for the following properties:
 * 		1. Associative
 * 		3. x mod gcd == 0
 * 		6. Multiplicative
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Gcd_1_Test.class, Gcd_3_Test.class,
		Gcd_6_Test.class })
public class Gcd_TestSuite_1_3_6 {

}
