package gcd.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gcd.tests.Gcd_2_Test;
import gcd.tests.Gcd_5_Test;
import gcd.tests.Gcd_6_Test;

/**
 * A testsuite for the following properties:
 * 		2. Commutative
 * 		5. Biggest
 * 		6. Multiplicative
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Gcd_2_Test.class, Gcd_5_Test.class,
		Gcd_6_Test.class })
public class Gcd_TestSuite_2_5_6 {

}
