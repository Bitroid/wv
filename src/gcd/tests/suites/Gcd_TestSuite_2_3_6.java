package gcd.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gcd.tests.Gcd_2_Test;
import gcd.tests.Gcd_3_Test;
import gcd.tests.Gcd_6_Test;

/**
 * A testsuite for the following properties:
 * 		2. Commutative
 * 		3. x mod gcd == 0
 * 		6. Multiplicative
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Gcd_2_Test.class, Gcd_3_Test.class,
		Gcd_6_Test.class })
public class Gcd_TestSuite_2_3_6 {

}
