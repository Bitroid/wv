package gcd.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gcd.tests.Gcd_1_Test;
import gcd.tests.Gcd_5_Test;

/**
 * A testsuite for the following properties:
 * 		1. Associative
 * 		5. Biggest
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Gcd_1_Test.class, Gcd_5_Test.class })
public class Gcd_TestSuite_1_5 {

}
