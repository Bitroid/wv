package gcd.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import gcd.tests.Gcd_1_Test;
import gcd.tests.Gcd_2_Test;
import gcd.tests.Gcd_4_Test;

/**
 * A testsuite for the following properties:
 * 		1. Associative
 * 		2. Commutative
 * 		4. y mod gcd == 0
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Gcd_1_Test.class, Gcd_2_Test.class,
		Gcd_4_Test.class })
public class Gcd_TestSuite_1_2_4 {

}
