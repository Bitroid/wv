package gcd.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * A testclass containing a test for the sixth property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class Gcd_6_Test {

	/**
	 * Testing the sixth property: multiplicative.
	 */
	@Test
	public void testMultiplicatief(){
		  assertTrue(GcdTestUtility.getObjectOfClassToTest().gcdIterative(15, 30) == 3 * GcdTestUtility.getObjectOfClassToTest().gcdIterative(5, 10));
	}
}
