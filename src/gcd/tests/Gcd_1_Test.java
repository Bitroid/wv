package gcd.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * A testclass containing a test for the first property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class Gcd_1_Test {

	/**
	 * Testing the first property: associative.
	 */
	@Test
	public void testAssociatief(){
		  assertTrue(GcdTestUtility.getObjectOfClassToTest().gcdIterative(GcdTestUtility.getObjectOfClassToTest().gcdIterative(16, 8), 4) == GcdTestUtility.getObjectOfClassToTest().gcdIterative(16, GcdTestUtility.getObjectOfClassToTest().gcdIterative(8, 4)));
	}
}
