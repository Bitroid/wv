package gcd.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * A testclass containing a test for the third property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class Gcd_3_Test {
	
	/**
	 * Testing the third property: x mod gcd == 0.
	 */
	@Test
	public void testDeeltP(){
		assertTrue((1052 % GcdTestUtility.getObjectOfClassToTest().gcdIterative(1052,  52) == 0));
	}
}
