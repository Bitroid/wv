package gcd.tests;

import wv.TestUtility;
import gcd.programs.GcdInterface;
import gcd.programs.proper.GcdCorrect;

/**
 * A class representing the test utility for the gcd-experiment.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class GcdTestUtility implements TestUtility {

	/**
	 * The tester for this experiment.
	 */
	public static Class<?> classToTest = GcdCorrect.class;	
	

	public static GcdInterface getObjectOfClassToTest() {
		try {
			return (GcdInterface) classToTest.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public void setClassToTest(Class<?> newClass) {
		classToTest = newClass;
		
	}
}
