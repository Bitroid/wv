package sorting;

import java.util.*;

import wv.TestAllVariations;
import sorting.programs.bad.*;
import sorting.tests.*;
import sorting.tests.SortingTestUtility;

public class TestAllVariationsImpl {
	
	

	public static void main(String args[])
	{
		List<Class<?>> classes;
		List<Class<?>> programClasses;
		classes = new ArrayList<Class<?>>();
		classes.add(SortingInputContainsOutputTest.class);
		classes.add(SortingInputCorrectFrequencyTest.class);
		classes.add(SortingOutputContainsInputTest.class);
		classes.add(SortingOutputCorrectFrequencyTest.class);
		classes.add(SortingPermutationTest.class);
		classes.add(SortingSameLengthTest.class);
		classes.add(SortingSortedTest.class);
		classes.add(SortingTest2_1_3.class);
		classes.add(SortingTest2_1_4.class);
		classes.add(SortingTest2_1_6.class);
		classes.add(SortingTest2_1_7.class);
		classes.add(SortingTest2_2_3.class);
		classes.add(SortingTest2_3_6.class);
		classes.add(SortingTest2_3_7.class);
		classes.add(SortingTest2_6_7.class);
		classes.add(SortingTest3_1_2_3.class);
		classes.add(SortingTest3_1_3_4.class);
		classes.add(SortingTest3_1_3_6.class);
		classes.add(SortingTest3_1_3_7.class);
		classes.add(SortingTest3_1_4_6.class);
		classes.add(SortingTest3_1_4_7.class);
		classes.add(SortingTest3_1_6_7.class);
		classes.add(SortingTest3_2_3_6.class);
		classes.add(SortingTest3_2_3_7.class);
		classes.add(SortingTest3_3_6_7.class);
		classes.add(SortingTest4_1_2_3_4.class);
		classes.add(SortingTest4_1_2_3_6.class);
		classes.add(SortingTest4_1_2_3_7.class);
		classes.add(SortingTest4_1_3_4_6.class);
		classes.add(SortingTest4_1_3_4_7.class);
		classes.add(SortingTest4_1_3_6_7.class);
		classes.add(SortingTest4_1_4_6_7.class);
		classes.add(SortingTest4_2_3_6_7.class);
		classes.add(SortingTest5_1_2_3_4_6.class);
		classes.add(SortingTest5_1_2_3_4_7.class);
		classes.add(SortingTest5_1_2_3_6_7.class);
		classes.add(SortingTest5_1_3_4_6_7.class);
		classes.add(SortingTest6_1_2_3_4_5_6.class);
		classes.add(SortingTest6_1_2_3_4_6_7.class);
		classes.add(SortingTest7_1_2_3_4_5_6_7.class);
		programClasses = new ArrayList<Class<?>>();
		programClasses.add(Sorting1_1.class);
		programClasses.add(Sorting1_3.class);
		programClasses.add(Sorting1_4.class);
		programClasses.add(Sorting1_5.class);
		programClasses.add(Sorting2_1_3.class);
		programClasses.add(Sorting2_1_4.class);
		programClasses.add(Sorting2_1_5.class);
		programClasses.add(Sorting2_1_6.class);
		programClasses.add(Sorting2_1_7.class);
		programClasses.add(Sorting2_3_4.class);
		programClasses.add(Sorting2_3_5.class);
		programClasses.add(Sorting2_3_6.class);
		programClasses.add(Sorting2_3_7.class);
		programClasses.add(Sorting2_4_5.class);
		programClasses.add(Sorting2_4_6.class);
		programClasses.add(Sorting3_1_3_4.class);
		programClasses.add(Sorting3_1_3_5.class);
		programClasses.add(Sorting3_1_3_7.class);
		programClasses.add(Sorting3_1_4_6.class);
		programClasses.add(Sorting3_3_4_5.class);
		programClasses.add(Sorting4_1_3_4_5.class);
		programClasses.add(Sorting6_2_3_4_5_6_7.class);
		programClasses.add(SortingTreeSort.class);
		
		TestAllVariations.testAllVariations(new SortingTestUtility(), classes, programClasses);
		
	}

}
