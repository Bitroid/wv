package sorting.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


import org.junit.Test;


public class SortingSortedTest {

	@Test
	public void sortedTest() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(3);
		input.add(2);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isSorted(output));
	}
	
	@Test
	public void sortedTest2() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(3);
		input.add(6);
		input.add(2);
		input.add(3);
		input.add(5);
		input.add(3);
		input.add(7);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isSorted(output));
	}
	
	@Test
	public void sortedTest3() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(19);
		input.add(3);
		input.add(23);
		input.add(9);
		input.add(20);
		input.add(41);
		input.add(3);
		input.add(2);
		input.add(19);
		input.add(21);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isSorted(output));
	}
	
	@Test
	public void sortedTest4() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(9);
		input.add(8);
		input.add(7);
		input.add(6);
		input.add(5);
		input.add(4);
		input.add(3);
		input.add(2);
		input.add(1);
		input.add(0);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isSorted(output));
	}

}
