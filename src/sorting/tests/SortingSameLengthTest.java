package sorting.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SortingSameLengthTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(2);
		input.add(2);
		input.add(3);
		input.add(4);
		input.add(5);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(output.size() == original.size());
	}
	
	@Test
	public void test2() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(11);
		input.add(27);
		input.add(28);
		input.add(0);
		input.add(0);
		input.add(0);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(output.size() == original.size());
	}
	
	@Test
	public void test3() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(27);
		input.add(28);
		input.add(0);
		input.add(11);
		input.add(27);
		input.add(28);
		input.add(0);
		input.add(7);
		input.add(0);
		input.add(2);
		input.add(5);
		input.add(4);
		input.add(28);
		input.add(0);
		input.add(2);
		input.add(3);
		input.add(4);
		input.add(10);
		input.add(9);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(output.size() == original.size());
	}
}
