package sorting.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


import org.junit.Test;

public class SortingPermutationTest {
	
	@Test
	public void permutTest() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(3);
		input.add(2);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isPermutation(original, output));
	}
	
	@Test
	public void permutTest2() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(2);
		input.add(2);
		input.add(5);
		input.add(3);
		input.add(2);
		input.add(6);
		input.add(7);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isPermutation(original, output));
	}
	
	@Test
	public void permutTest3() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(2);
		input.add(99);
		input.add(3);
		input.add(10);
		input.add(2);
		input.add(75615);
		input.add(65);
		input.add(9);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isPermutation(original, output));
	}
	
	@Test
	public void permutTest4() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(3);
		input.add(2);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(3);
		input.add(2);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(3);
		input.add(2);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.isPermutation(original, output));
	}
}
