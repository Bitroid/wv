package sorting.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SortingOutputContainsInputTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(2);
		input.add(2);
		input.add(3);
		input.add(4);
		input.add(5);
		input.add(7);
		input.add(9);
		input.add(10);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.containsAll(output, original));
	}
	
	@Test
	public void test2() {
		ArrayList<Integer> input = new ArrayList<Integer>(5);
		input.add(1);
		input.add(3);
		input.add(2);
		input.add(6);
		input.add(6);
		input.add(-1);
		input.add(7);
		input.add(24);
		input.add(10);
		List<Integer> original = new ArrayList<Integer>(input);
		List<Integer> output = SortingTestUtility.getObjectOfClassToTest().sort(input);
		assertTrue(SortingTestUtility.containsAll(output, original));
	}


}
