package sorting.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
sorting.tests.SortingInputCorrectFrequencyTest.class,
sorting.tests.SortingOutputContainsInputTest.class,
sorting.tests.SortingSameLengthTest.class })
public class SortingTest3_2_3_6 {

}
