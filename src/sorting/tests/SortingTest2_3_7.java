package sorting.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
sorting.tests.SortingOutputContainsInputTest.class,
sorting.tests.SortingSortedTest.class })
public class SortingTest2_3_7 {

}
