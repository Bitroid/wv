package sorting.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
sorting.tests.SortingInputContainsOutputTest.class,
sorting.tests.SortingOutputContainsInputTest.class,
sorting.tests.SortingOutputCorrectFrequencyTest.class,
sorting.tests.SortingSameLengthTest.class,
sorting.tests.SortingSortedTest.class })
public class SortingTest5_1_3_4_6_7 {

}
