package sorting.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
sorting.tests.SortingInputContainsOutputTest.class,
sorting.tests.SortingInputCorrectFrequencyTest.class,
sorting.tests.SortingOutputContainsInputTest.class })
public class SortingTest3_1_2_3 {

}
