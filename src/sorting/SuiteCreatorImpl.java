package sorting;
import java.util.*;

import sorting.tests.*;

import wv.SuiteCreator;

public class SuiteCreatorImpl {

	public static String PACKAGEDIR = "src/sorting/tests/suites/";
	public static String PACKAGE = "sorting.tests.suites";
	public static String CLASSPREFIX = "SortingTest";

	public static void main(String args[])
	{
		
		try{
			List<Class<?>> classes = new ArrayList<Class<?>>();
			
			classes.add(SortingInputContainsOutputTest.class);
			classes.add(SortingInputCorrectFrequencyTest.class);
			classes.add(SortingOutputContainsInputTest.class);
			classes.add(SortingOutputCorrectFrequencyTest.class);
			classes.add(SortingPermutationTest.class);
			classes.add(SortingSameLengthTest.class);
			classes.add(SortingSortedTest.class);
			
			Map<Class<?>, List<Class<?>>> redundancies = new HashMap<Class<?>, List<Class<?>>>();
			List<Class<?>> list = new ArrayList<Class<?>>();
			list.add(SortingInputContainsOutputTest.class);
			list.add(SortingInputContainsOutputTest.class);
			list.add(SortingInputCorrectFrequencyTest.class);
			list.add(SortingOutputContainsInputTest.class);
			list.add(SortingOutputCorrectFrequencyTest.class);
			list.add(SortingSameLengthTest.class);
			redundancies.put(SortingPermutationTest.class, list);
			
			list = new ArrayList<Class<?>>();
			list.add(SortingInputContainsOutputTest.class);
			redundancies.put(SortingOutputCorrectFrequencyTest.class, list);
			
			list = new ArrayList<Class<?>>();
			list.add(SortingOutputContainsInputTest.class);
			redundancies.put(SortingInputCorrectFrequencyTest.class, list);
			
			SuiteCreator.createSuites(PACKAGE, CLASSPREFIX, classes, redundancies);

		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
}
