package sorting;
import java.util.*;
import wv.MutationTester;

public class MutationTesterImpl {

	public static String TESTPACKAGE = "sorting.tests";
	public static String PROGRAMPACKAGE = "sorting.programs.proper";
	public static long startTime = System.currentTimeMillis();

	public static void main(String args[]) 
	{ 

		List<String> classes = new ArrayList<String>();
		classes.add("SortingInputContainsOutputTest");
		classes.add("SortingInputCorrectFrequencyTest");
		classes.add("SortingOutputContainsInputTest");
		classes.add("SortingOutputCorrectFrequencyTest");
		classes.add("SortingPermutationTest");
		classes.add("SortingSameLengthTest");
		classes.add("SortingSortedTest");
		classes.add("SortingTest2_1_3");
		classes.add("SortingTest2_1_4");
		classes.add("SortingTest2_1_6");
		classes.add("SortingTest2_1_7");
		classes.add("SortingTest2_2_3");
		classes.add("SortingTest2_3_6");
		classes.add("SortingTest2_3_7");
		classes.add("SortingTest2_6_7");
		classes.add("SortingTest3_1_2_3");
		classes.add("SortingTest3_1_3_4");
		classes.add("SortingTest3_1_3_6");
		classes.add("SortingTest3_1_3_7");
		classes.add("SortingTest3_1_4_6");
		classes.add("SortingTest3_1_4_7");
		classes.add("SortingTest3_1_6_7");
		classes.add("SortingTest3_2_3_6");
		classes.add("SortingTest3_2_3_7");
		classes.add("SortingTest3_3_6_7");
		classes.add("SortingTest4_1_2_3_4");
		classes.add("SortingTest4_1_2_3_6");
		classes.add("SortingTest4_1_2_3_7");
		classes.add("SortingTest4_1_3_4_6");
		classes.add("SortingTest4_1_3_4_7");
		classes.add("SortingTest4_1_3_6_7");
		classes.add("SortingTest4_1_4_6_7");
		classes.add("SortingTest4_2_3_6_7");
		classes.add("SortingTest5_1_2_3_4_6");
		classes.add("SortingTest5_1_2_3_4_7");
		classes.add("SortingTest5_1_2_3_6_7");
		classes.add("SortingTest5_1_3_4_6_7");
		classes.add("SortingTest6_1_2_3_4_5_6");
		classes.add("SortingTest6_1_2_3_4_6_7");
		classes.add("SortingTest7_1_2_3_4_5_6_7");
		
		MutationTester.calculateMAS(TESTPACKAGE, PROGRAMPACKAGE, classes);
	}

}
