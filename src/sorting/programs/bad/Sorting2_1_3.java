package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import sorting.programs.Sorting;

public class Sorting2_1_3 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		for (int i = 0; set.size() <= input.size(); i++)
			set.add(i);
		return new ArrayList<Integer>(set);
	}

}
