package sorting.programs.bad;

import java.util.*;

import sorting.programs.Sorting;

public class Sorting1_4 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		ArrayList<Integer> reversed = reverse(new ArrayList<Integer>(set));
		for (int i = reversed.size(); i <= input.size(); i++)
			reversed.add(input.get(i));
		return reversed;
	}
	
	private ArrayList<Integer> reverse(ArrayList<Integer> input) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i = input.size() - 1; i > 0; i--)
			result.add(input.get(i));
		return result;
	}

}
