package sorting.programs.bad;

import java.util.*;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting1_3 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		ArrayList<Integer> reversed = reverse(new ArrayList<Integer>(set));
		for (int i = reversed.size(); i <= input.size(); i++)
			reversed.add(i);
		return reversed;
	}
	
	private ArrayList<Integer> reverse(ArrayList<Integer> input) {
		ArrayList<Integer> result = new SortingMerge().sort(input);
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}

}
