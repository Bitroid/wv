package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting2_4_6 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Sorting sorter = new SortingMerge();
		List<Integer> result = sorter.sort(input);
		result = reverse(result);
		List<Integer> toRemove = new ArrayList<Integer>();
		toRemove.add(result.get(0));
		result.removeAll(toRemove);
		return result;
	}
	
	private List<Integer> reverse(List<Integer> input) {
		List<Integer> result = new SortingMerge().sort(input);
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}

}
