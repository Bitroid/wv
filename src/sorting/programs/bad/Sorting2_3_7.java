package sorting.programs.bad;

import java.util.List;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting2_3_7 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Sorting sorter = new SortingMerge();
		List<Integer> result = sorter.sort(input);
		result = reverse(result);
		boolean found = false;
		int i = 0;
		while (!found) {
			if (!input.contains(i)) {
				result.add(i);
				found = true;
			}
			i++;
		}
		
		return result;
	}
	
	private List<Integer> reverse(List<Integer> input) {
		List<Integer> result = new SortingMerge().sort(input);
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}

}
