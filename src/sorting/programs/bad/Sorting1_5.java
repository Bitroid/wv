package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;

import sorting.programs.Sorting;

public class Sorting1_5 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(i);
		return result;
	}

}
