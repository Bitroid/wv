package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting4_1_3_4_5 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		List<Integer> copy = new ArrayList<Integer>(set);
		int i = 0;
		while (copy.size() < input.size()) {
			copy.add(input.get(i));
			i++;
		}
		Sorting sorter = new SortingMerge();
		return sorter.sort(copy);
	}

}
