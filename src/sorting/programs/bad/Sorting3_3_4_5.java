package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import sorting.programs.Sorting;

public class Sorting3_3_4_5 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		List<Integer> copy = new ArrayList<Integer>(set);
		int i = 0;
		while (copy.size() < input.size()) {
			copy.add(input.get(i));
			i++;
		}
		ArrayList<Integer> reversed = reverse(copy);
		return reversed;
	}
	
	private ArrayList<Integer> reverse(List<Integer> input) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}

}
