package sorting.programs.bad;
import java.util.*;

import sorting.programs.Sorting;

public class SortingTreeSort implements Sorting {

	public List<Integer> sort(List<Integer> input) {
		TreeSet<Integer> set = new TreeSet<Integer>(input);
		return new ArrayList<Integer>(set);
	}

}