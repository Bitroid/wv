package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting2_1_6 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Sorting sorter = new SortingMerge();
		List<Integer> copy = new ArrayList<Integer>(input);
		List<Integer> toRemove = new ArrayList<Integer>();
		toRemove.add(copy.get(0));
		copy.removeAll(toRemove);
		List<Integer> result = sorter.sort(copy);
		return result;
	}

}
