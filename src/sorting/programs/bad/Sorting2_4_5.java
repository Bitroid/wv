package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import sorting.programs.Sorting;

public class Sorting2_4_5 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		ArrayList<Integer> reversed = reverse(new ArrayList<Integer>(set));
		reversed.remove(0);
		for (int i = 0; reversed.size() < input.size(); i++)
			reversed.add(reversed.get(i));
		return reversed;
	}
	
	private ArrayList<Integer> reverse(ArrayList<Integer> input) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}

}
