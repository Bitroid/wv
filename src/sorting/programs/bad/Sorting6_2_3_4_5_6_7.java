package sorting.programs.bad;

import java.util.List;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting6_2_3_4_5_6_7 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		return reverse(input);
	}
	
	private List<Integer> reverse(List<Integer> input) {
		List<Integer> result = new SortingMerge().sort(input);
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}
	

}
