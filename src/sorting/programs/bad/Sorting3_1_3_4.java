package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting3_1_3_4 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		List<Integer> list = new ArrayList<Integer>(set);
		list.addAll(set);
		Sorting sorter = new SortingMerge();
		return sorter.sort(list);
	}

}
