package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting3_1_3_7 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Sorting sorter = new SortingMerge();
		List<Integer> copy = new ArrayList<Integer>(input);
		boolean found = false;
		int i = 0;
		while (!found) {
			if (!input.contains(i)) {
				copy.add(i);
				found = true;
			}
			i++;
		}
		
		return sorter.sort(copy);
	}

}
