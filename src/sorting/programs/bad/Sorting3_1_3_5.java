package sorting.programs.bad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import sorting.programs.Sorting;
import sorting.programs.proper.SortingMerge;

public class Sorting3_1_3_5 implements Sorting {
	
	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		int i = 0;
		while (set.size() < input.size()) {
			if (!input.contains(i))
				set.add(i);
			i++;
		}
		Sorting sorter = new SortingMerge();
		return sorter.sort(new ArrayList<Integer>(set));
	}

}
