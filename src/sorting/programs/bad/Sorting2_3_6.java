package sorting.programs.bad;

import java.util.*;

import sorting.programs.Sorting;

public class Sorting2_3_6 implements Sorting {

	@Override
	public List<Integer> sort(List<Integer> input) {
		Set<Integer> set = new TreeSet<Integer>(input);
		List<Integer> reversed = reverse(new ArrayList<Integer>(set));
		for (int i = reversed.size(); i <= input.size(); i++)
			reversed.add(i);
		return reversed;
	}
	
	private List<Integer> reverse(ArrayList<Integer> input) {
		List<Integer> result = new SortingProper().sort(input);
		for (int i = input.size() - 1; i >= 0; i--)
			result.add(input.get(i));
		return result;
	}

}
