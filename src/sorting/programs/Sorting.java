package sorting.programs;

import java.util.List;

public interface Sorting {

	public List<Integer> sort(List<Integer> input);
	
    /*
     * Eigenschappen:
     * - Output is gesorteerd.
     * - Output is permutatie van input.
     * - Output bevat alle getallen in input.
     * - Input bevat alle getallen in output.
     * - Output is even lang als input.
     * - Getallen in output komen even vaak voor als in input.
     * - Getallen in input komen even vaak voor als in output.
     */
	
	/*
	 * 1.
	 */
}
