package stringConcat.programs.proper;

import stringConcat.programs.SC_Interface;

/**
 * A class representing the correct version of concatenating strings.
 * 
 * Properties:
 * 		1. length of resulting string is s1+s2
 * 		2. in the resulting string, s1 comes before s2
 * 		3. every character of s1 has to be in the result
 * 		4. order of the characters of s1 in the resulting string has to be the same as before concatenating
 * 		5. order of the characters of s2 in the resulting string has to be the same as before concatenating
 * 		6. every character of s2 has to be in the result
 *
 *			breaking 3 => breaking 4
 *			breaking 6 => breaking 5
 *
 * @author	Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class StringConcatenation implements SC_Interface{
	
	/**
	 * Concatenating the two given strings.
	 */
	public char[] concatenate(char[] s1, char[] s2){
		if (s1.length == 0){
			return s2;
		}
		else if (s2.length == 0){
			return s1;
		}
		else {
			int s1Length = s1.length;
			int s2Length = s2.length;
			int totalLength = s1Length + s2Length;
			char[] result = new char[totalLength];
			for (int i = 0; i < s1Length ; i++){
				result[i] = s1[i];
			}
			int temp = 0;
			for (int j = s1Length; j < totalLength; j++){
				result[j] = s2[temp];
				temp++;
			}	
			return result;			
		}
	}
}