package stringConcat.programs.bad;

import stringConcat.programs.SC_Interface;

/**
 * Breaking the following properties of string concatenation:
 * 		1. length of resulting string is s1+s2
 * 		2. in the resulting string, s1 comes before s2
 * 		4. order of the characters of s1 in the resulting string has to be the same as before concatenating
 * 		5. order of the characters of s2 in the resulting string has to be the same as before concatenating
 * 		6. every character of s2 has to be in the result
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class SC_Breaking_1_2_4_5_6 implements SC_Interface{
	
	/**
	 * Computing the correct string concatenation of the two given strings.
	 */
	private char[] conc(char[] s1, char[] s2){
		if (s1.length == 0){
			return s2;
		}
		else if (s2.length == 0){
			return s1;
		}
		else {
			int s1Length = s1.length;
			int s2Length = s2.length;
			int totalLength = s1Length + s2Length;
			char[] result = new char[totalLength];
			for (int i = 0; i < s1Length ; i++){
				result[i] = s1[i];
			}
			int temp = 0;
			for (int j = s1Length; j < totalLength; j++){
				result[j] = s2[temp];
				temp++;
			}	
			return result;			
		}
	}
	
	/**
	 * Computing the string concatenation while breaking properties 1, 2, 4, 5 and 6.
	 */
	@Override
	public char[] concatenate(char[] s1, char[] s2){
		// breaking property 4
		if(s1.length != 1){
			char temp = s1[0];
			s1[0] = s1[1];
			s1[1] = temp;
		}
		// breaking property 5
		if(s2.length != 1){
			char temp = s2[0];
			s2[0] = s2[1];
			s2[1] = temp;
		}		
		// breaking property 6
		char[]subset2 = new char[s2.length-1];
		for(int i = 0; i < s2.length-1; i++){
			subset2[i] = s2[i];
		}
		// breaking property 2
		return conc(subset2, s1);
	}
}
