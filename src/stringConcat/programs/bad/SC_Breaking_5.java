package stringConcat.programs.bad;

import stringConcat.programs.SC_Interface;

/**
 * Breaking the fifth properties of string concatenation:
 * 		5. order of the characters of s2 in the resulting string has to be the same as before concatenating
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class SC_Breaking_5 implements SC_Interface{

	/**
	 * Computing the string concatenation while breaking the fifth property.
	 */
	@Override
	public char[] concatenate(char[] s1, char[] s2){
		if (s1.length == 0){
			return s2;
		}
		else if (s2.length == 0){
			return s1;
		}
		else {
			int s1Length = s1.length;
			int s2Length = s2.length;
			int totalLength = s1Length + s2Length;
			if(s2Length != 1){
				char charTemp;
				charTemp = s2[0];
				s2[0] = s2[1];
				s2[1] = charTemp;
			}
			
			char[] result = new char[totalLength];
			for (int i = 0; i < s1Length ; i++){
				result[i] = s1[i];
			}
			int temp = 0;
			for (int j = s1Length; j < totalLength; j++){
				result[j] = s2[temp];
				temp++;
			}
			return result;			
		}
	}
	
}
