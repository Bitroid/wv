package stringConcat.programs.bad;

import stringConcat.programs.SC_Interface;

/**
 * Breaking the second properties of string concatenation:
 * 		2. in the resulting string, s1 comes before s2
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class SC_Breaking_2 implements SC_Interface{

	/**
	 * Computing the string concatenation while breaking the second property.
	 */
	@Override
	public char[] concatenate(char[] s1, char[] s2){
		if (s1.length == 0){
			return s2;
		}
		else if (s2.length == 0){
			return s1;
		}
		else {
			int s1Length = s1.length;
			int s2Length = s2.length;
			int totalLength = s1Length + s2Length;
			char[] result = new char[totalLength];
			for (int i = 0; i < s2Length ; i++){
				result[i] = s2[i];
			}
			int temp = 0;
			for (int j = s2Length; j < totalLength; j++){
				result[j] = s1[temp];
				temp++;
			}	
			return result;			
		}
	}
	
}
