package stringConcat.programs.bad;

import stringConcat.programs.SC_Interface;

/**
 * Breaking the following properties of string concatenation:
 * 		1. length of resulting string is s1+s2
 * 		2. in the resulting string, s1 comes before s2
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class SC_Breaking_1_2 implements SC_Interface{

	/**
	 * Computing the correct string concatenation of the two given strings.
	 */
	private char[] conc(char[] s1, char[] s2){
		if (s1.length == 0){
			return s2;
		}
		else if (s2.length == 0){
			return s1;
		}
		else {
			int s1Length = s1.length;
			int s2Length = s2.length;
			int totalLength = s1Length + s2Length;
			char[] result = new char[totalLength];
			for (int i = 0; i < s1Length ; i++){
				result[i] = s1[i];
			}
			int temp = 0;
			for (int j = s1Length; j < totalLength; j++){
				result[j] = s2[temp];
				temp++;
			}	
			return result;			
		}
	}
	
	/**
	 * Computing the string concatenation while breaking the properties 1 and 2.
	 */
	@Override
	public char[] concatenate(char[] s1, char[] s2){
		// breaking property 2
		char[] result1 = conc(s2, s1);
		
		// breaking property 1
		char[] empty = {' '};
		return conc(result1, empty);
	}
	
}
