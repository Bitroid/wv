package stringConcat.programs.bad;

import stringConcat.programs.SC_Interface;

/**
 * Breaking the fourht properties of string concatenation:
 * 		4. order of the characters of s1 in the resulting string has to be the same as before concatenating
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public class SC_Breaking_4 implements SC_Interface{

	/**
	 * Computing the string concatenation while breaking the fourth property.
	 */
	@Override
	public char[] concatenate(char[] s1, char[] s2){
		if (s1.length == 0){
			return s2;
		}
		else if (s2.length == 0){
			return s1;
		}
		else {
			int s1Length = s1.length;
			int s2Length = s2.length;
			int totalLength = s1Length + s2Length;
						
			if(s1Length != 1){
				char charTemp;
				charTemp = s1[0];
				s1[0] = s1[1];
				s1[1] = charTemp;
			}
			char[] result = new char[totalLength];
			for (int i = 0; i < s1Length ; i++){
				result[i] = s1[i];
			}
			int temp = 0;
			for (int j = s1Length; j < totalLength; j++){
				result[j] = s2[temp];
				temp++;
			}
			return result;			
		}
	}
}
