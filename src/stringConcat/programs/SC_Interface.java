package stringConcat.programs;

/**
 * An interface for computing the string concatenation.
 *
 *
 *  * Properties:
 * 		1. length of resulting string is s1+s2
 * 		2. in the resulting string, s1 comes before s2
 * 		3. every character of s1 has to be in the result
 * 		4. order of the characters of s1 in the resulting string has to be the same as before concatenating
 * 		5. order of the characters of s2 in the resulting string has to be the same as before concatenating
 * 		6. every character of s2 has to be in the result
 *
 *			breaking 3 => breaking 4
 *			breaking 6 => breaking 5
 *
 *
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
public interface SC_Interface {
	public char[] concatenate(char[] s1, char[] s2);
}
