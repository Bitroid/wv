package stringConcat.tests;

import stringConcat.programs.SC_Interface;
import stringConcat.programs.proper.StringConcatenation;
import wv.TestUtility;

/**
 * A class representing the test utility for the string concatenation-experiment.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class SC_TestUtility implements TestUtility {

public static Class<?> classToTest = StringConcatenation.class;	
	
	public static SC_Interface getObjectOfClassToTest() {
		try {
			return (SC_Interface) classToTest.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public void setClassToTest(Class<?> newClass) {
		classToTest = newClass;
		
	}
}
