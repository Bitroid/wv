package stringConcat.tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * onderlinge volgorde van letters van s1 is dezelfde als die in result
 */
public class SC_4_Test {

	@Test
	public void test4(){
		char[] c1 = {'A', 'b', 'c'};
		char[] c2 = {'d', 'e', 'f', 'g'};
		char[] result = SC_TestUtility.getObjectOfClassToTest().concatenate(c1, c2);
		List<Character> result2 = new ArrayList<Character>();
		for (int i = 0; i < result.length; i++){
			result2.add(result[i]);
		}
		
		assertTrue(result2.indexOf('b') - result2.indexOf('A') == 1);
		assertTrue(result2.indexOf('c') - result2.indexOf('b') == 1);
		assertTrue(result2.indexOf('c') - result2.indexOf('A') == 2);
	}
	
}
