package stringConcat.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * A testclass containing a test for the second property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class SC_2_Test {

	/**
	 * Testing the second property.
	 */
	@Test
	public void test2(){
		char[] c1 = {'A', 'b', 'c'};
		char[] c2 = {'d', 'e'};
		char[] result = SC_TestUtility.getObjectOfClassToTest().concatenate(c1, c2);
		List<Character> result2 = new ArrayList<Character>();
		for (int i = 0; i < result.length; i++){
			result2.add(result[i]);
		}
		assertTrue(result2.indexOf('A') < result2.indexOf('d'));
		assertTrue(result2.indexOf('A') < result2.indexOf('d'));
		assertTrue(result2.indexOf('A') < result2.indexOf('d'));
		
		assertTrue(result2.indexOf('b') < result2.indexOf('d'));
		assertTrue(result2.indexOf('b') < result2.indexOf('d'));
		assertTrue(result2.indexOf('b') < result2.indexOf('d'));
		
		assertTrue(result2.indexOf('c') < result2.indexOf('d'));
		assertTrue(result2.indexOf('c') < result2.indexOf('d'));
		assertTrue(result2.indexOf('c') < result2.indexOf('d'));
	}
	
}
