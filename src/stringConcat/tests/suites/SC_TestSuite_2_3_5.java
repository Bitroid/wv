package stringConcat.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import stringConcat.tests.SC_2_Test;
import stringConcat.tests.SC_3_Test;
import stringConcat.tests.SC_5_Test;

/**
 * A testsuite for the following properties:
 * 		2. in the resulting string, s1 comes before s2
 * 		3. every character of s1 has to be in the result
 * 		5. order of the characters of s2 in the resulting string has to be the same as before concatenating
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ SC_2_Test.class, SC_3_Test.class, SC_5_Test.class })
public class SC_TestSuite_2_3_5 {

}
