package stringConcat.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import stringConcat.tests.SC_3_Test;
import stringConcat.tests.SC_4_Test;
import stringConcat.tests.SC_6_Test;

/**
 * A testsuite for the following properties:
 * 		3. every character of s1 has to be in the result
 * 		4. order of the characters of s1 in the resulting string has to be the same as before concatenating
 * 		6. every character of s2 has to be in the result
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ SC_3_Test.class, SC_4_Test.class, SC_6_Test.class })
public class SC_TestSuite_3_4_6 {

}
