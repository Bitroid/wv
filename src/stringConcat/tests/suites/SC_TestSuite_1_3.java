package stringConcat.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import stringConcat.tests.SC_1_Test;
import stringConcat.tests.SC_3_Test;

/**
 * A testsuite for the following properties:
 * 		1. length of resulting string is s1+s2
 * 		3. every character of s1 has to be in the result
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ SC_1_Test.class, SC_3_Test.class})
public class SC_TestSuite_1_3 {

}
