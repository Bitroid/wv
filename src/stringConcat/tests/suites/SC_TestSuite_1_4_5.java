package stringConcat.tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import stringConcat.tests.SC_1_Test;
import stringConcat.tests.SC_4_Test;
import stringConcat.tests.SC_5_Test;

/**
 * A testsuite for the following properties:
 * 		1. length of resulting string is s1+s2
 * 		4. order of the characters of s1 in the resulting string has to be the same as before concatenating
 * 		5. order of the characters of s2 in the resulting string has to be the same as before concatenating
 * 		
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ SC_1_Test.class, SC_4_Test.class, SC_5_Test.class })
public class SC_TestSuite_1_4_5 {

}
