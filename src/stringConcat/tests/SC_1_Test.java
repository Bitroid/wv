package stringConcat.tests;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * A testclass containing a test for the first property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class SC_1_Test {

	/**
	 * Testing the first property.
	 */
	@Test
	public void test1(){
		char[] c1 = {'A', 'b', 'c'};
		char[] c2 = {'d', 'e', 'f', 'g'};
		int length = c1.length + c2.length;
		char[] result = SC_TestUtility.getObjectOfClassToTest().concatenate(c1, c2);
		assertTrue(result.length == length);
	}
	
}
