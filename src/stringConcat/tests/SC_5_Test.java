package stringConcat.tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * A testclass containing a test for the fifth property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class SC_5_Test {

	/**
	 * Testing the fifth property.
	 */
	@Test
	public void test5(){
		char[] c1 = {'A', 'b', 'c'};
		char[] c2 = {'d', 'e', 'f', 'g'};
		char[] result = SC_TestUtility.getObjectOfClassToTest().concatenate(c1, c2);
		List<Character> result2 = new ArrayList<Character>();
		for (int i = 0; i < result.length; i++){
			result2.add(result[i]);
		}
		
		assertTrue(result2.indexOf('g') - result2.indexOf('f') == 1);
		assertTrue(result2.indexOf('f') - result2.indexOf('e') == 1);
		assertTrue(result2.indexOf('e') - result2.indexOf('d') == 1);
		
		assertTrue(result2.indexOf('g') - result2.indexOf('e') == 2);
		assertTrue(result2.indexOf('f') - result2.indexOf('d') == 2);
		
		assertTrue(result2.indexOf('g') - result2.indexOf('d') == 3);
	}
	
	
}
