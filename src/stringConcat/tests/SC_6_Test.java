package stringConcat.tests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;


/**
 * elke letter van s1 in result zitten
 */
public class SC_6_Test {

	 private static HashMap<Character, Integer> countStringOccurences(char[] charArray) {
	        HashMap<Character, Integer> countMap = new HashMap<Character, Integer>();
	        for (char c : charArray) {
	            if (!countMap.containsKey(c)) {
	                countMap.put(c, 1);
	            } else {
	                Integer count = countMap.get(c);
	                count = count + 1;
	                countMap.put(c, count);
	            }
	        }
	        return countMap;
	}
	
	@Test
	public void test3(){
		char[] c1 = {'A', 'b', 'b'};
		char[] c2 = {'d', 'd', 'e', 'e', 'f'};
		char[] result = SC_TestUtility.getObjectOfClassToTest().concatenate(c1, c2);
		HashMap<Character, Integer> occResult = countStringOccurences(result);
		assertTrue(occResult.get('d') == 2);
		assertTrue(occResult.get('e') == 2);
		assertTrue(occResult.get('f') == 1);
	}
	
}
