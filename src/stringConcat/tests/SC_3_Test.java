package stringConcat.tests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;


/**
 * A testclass containing a test for the third property.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class SC_3_Test {

	/**
	 * Counting the number of occurences of a character in a string.
	 */
	 private static HashMap<Character, Integer> countStringOccurences(char[] charArray) {
	        HashMap<Character, Integer> countMap = new HashMap<Character, Integer>();
	        for (char c : charArray) {
	            if (!countMap.containsKey(c)) {
	                countMap.put(c, 1);
	            } else {
	                Integer count = countMap.get(c);
	                count = count + 1;
	                countMap.put(c, count);
	            }
	        }
	        return countMap;
	}
	
	/**
	 * Testing the third property.
	 */
	@Test
	public void test3(){
		char[] c1 = {'A', 'b', 'b'};
		char[] c2 = {'D', 'c', 'e'};
		char[] result = SC_TestUtility.getObjectOfClassToTest().concatenate(c1, c2);
		HashMap<Character, Integer> occResult = countStringOccurences(result);
		assertTrue(occResult.get('A') == 1);
		assertTrue(occResult.get('b') == 2);
	}
	
}
