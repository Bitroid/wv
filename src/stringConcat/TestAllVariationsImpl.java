package stringConcat;

import java.util.*;

import wv.TestAllVariations;
import stringConcat.programs.bad.*;
import stringConcat.tests.*;
import stringConcat.tests.suites.*;

public class TestAllVariationsImpl {
	
	

	public static void main(String args[])
	{
		List<Class<?>> classes;
		List<Class<?>> programClasses;
		classes = new ArrayList<Class<?>>();
		classes.add(SC_1_Test.class);
		classes.add(SC_2_Test.class);
		classes.add(SC_3_Test.class);
		classes.add(SC_4_Test.class);
		classes.add(SC_5_Test.class);
		classes.add(SC_6_Test.class);
		classes.add(SC_TestSuite_1_2_3_4_6.class);
		classes.add(SC_TestSuite_1_2_3_4.class);
		classes.add(SC_TestSuite_1_2_3_5_6.class);
		classes.add(SC_TestSuite_1_2_3_5.class);
		classes.add(SC_TestSuite_1_2_3_6.class);
		classes.add(SC_TestSuite_1_2_3.class);
		classes.add(SC_TestSuite_1_2_4_5_6.class);
		classes.add(SC_TestSuite_1_2_4_5.class);
		classes.add(SC_TestSuite_1_2_4_6.class);
		classes.add(SC_TestSuite_1_2_4.class);
		classes.add(SC_TestSuite_1_2_5_6.class);
		classes.add(SC_TestSuite_1_2_5.class);
		classes.add(SC_TestSuite_1_2_6.class);
		classes.add(SC_TestSuite_1_2.class);
		classes.add(SC_TestSuite_1_3_4_5_6.class);
		classes.add(SC_TestSuite_1_3_4_5.class);
		classes.add(SC_TestSuite_1_3_4_6.class);
		classes.add(SC_TestSuite_1_3_4.class);
		classes.add(SC_TestSuite_1_3_5_6.class);
		classes.add(SC_TestSuite_1_3_5.class);
		classes.add(SC_TestSuite_1_3_6.class);
		classes.add(SC_TestSuite_1_3.class);
		classes.add(SC_TestSuite_1_4_5_6.class);
		classes.add(SC_TestSuite_1_4_5.class);
		classes.add(SC_TestSuite_1_4_6.class);
		classes.add(SC_TestSuite_1_4.class);
		classes.add(SC_TestSuite_1_5_6.class);
		classes.add(SC_TestSuite_1_5.class);
		classes.add(SC_TestSuite_1_6.class);
		classes.add(SC_TestSuite_2_3_4_5_6.class);
		classes.add(SC_TestSuite_2_3_4_5.class);
		classes.add(SC_TestSuite_2_3_4_6.class);
		classes.add(SC_TestSuite_2_3_4.class);
		classes.add(SC_TestSuite_2_3_5_6.class);
		classes.add(SC_TestSuite_2_3_5.class);
		classes.add(SC_TestSuite_2_3_6.class);
		classes.add(SC_TestSuite_2_3.class);
		classes.add(SC_TestSuite_2_4_5_6.class);
		classes.add(SC_TestSuite_2_4_6.class);
		classes.add(SC_TestSuite_2_4.class);
		classes.add(SC_TestSuite_2_5_6.class);
		classes.add(SC_TestSuite_2_5.class);
		classes.add(SC_TestSuite_2_6.class);
		classes.add(SC_TestSuite_3_4_5_6.class);
		classes.add(SC_TestSuite_3_4_5.class);
		classes.add(SC_TestSuite_3_4_6.class);
		classes.add(SC_TestSuite_3_4.class);
		classes.add(SC_TestSuite_3_5_6.class);
		classes.add(SC_TestSuite_3_5.class);
		classes.add(SC_TestSuite_3_6.class);
		classes.add(SC_TestSuite_4_5_6.class);
		classes.add(SC_TestSuite_4_5.class);
		classes.add(SC_TestSuite_4_6.class);
		classes.add(SC_TestSuite_5_6.class);
		classes.add(SC_TestSuiteAll.class);
		
		programClasses = new ArrayList<Class<?>>();
		programClasses.add(SC_Breaking_1_2_3_4_5_6.class);
		programClasses.add(SC_Breaking_1_2_3_4_5.class);
		programClasses.add(SC_Breaking_1_2_3_4.class);
		programClasses.add(SC_Breaking_1_2_4_5_6.class);
		programClasses.add(SC_Breaking_1_2_4.class);
		programClasses.add(SC_Breaking_1_2_5_6.class);
		programClasses.add(SC_Breaking_1_2_5.class);
		programClasses.add(SC_Breaking_1_2.class);
		programClasses.add(SC_Breaking_1_3_4.class);
		programClasses.add(SC_Breaking_1_4_5_6.class);
		programClasses.add(SC_Breaking_1_4_5.class);
		programClasses.add(SC_Breaking_1_4.class);
		programClasses.add(SC_Breaking_1_5_6.class);
		programClasses.add(SC_Breaking_1_5.class);
		programClasses.add(SC_Breaking_1.class);
		programClasses.add(SC_Breaking_2_3_4_5_6.class);
		programClasses.add(SC_Breaking_2_3_4_5.class);
		programClasses.add(SC_Breaking_2_3_4.class);
		programClasses.add(SC_Breaking_2_4_5_6.class);
		programClasses.add(SC_Breaking_2_4_5.class);
		programClasses.add(SC_Breaking_2_4.class);
		programClasses.add(SC_Breaking_2_5_6.class);
		programClasses.add(SC_Breaking_2_5.class);
		programClasses.add(SC_Breaking_2.class);
		programClasses.add(SC_Breaking_3_4_5_6.class);
		programClasses.add(SC_Breaking_3_4_5.class);
		programClasses.add(SC_Breaking_3_4.class);
		programClasses.add(SC_Breaking_4_5_6.class);
		programClasses.add(SC_Breaking_4_5.class);
		programClasses.add(SC_Breaking_4.class);
		programClasses.add(SC_Breaking_5_6.class);
		programClasses.add(SC_Breaking_5.class);
		
		TestAllVariations.testAllVariations(new SC_TestUtility(), classes, programClasses);
		
	}

}
