package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanAvg2LargerThanMeanTest.class,
		BAMeanDistanceToAvgAbvTest.class, BAMeanDistanceToMeanTest.class })
public class BAMeanTest3_1_4 {

}
