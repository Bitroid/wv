package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanAvg1SmallerThanMean.class,
		BAMeanAvg2LargerThanMeanTest.class, BAMeanDistanceToMeanTest.class })
public class BAMeanTest3_3_4 {

}
