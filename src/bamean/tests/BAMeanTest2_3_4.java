package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanDistanceToAvgAbvTest.class,
		BAMeanDistanceToAvgBelowTest.class })
public class BAMeanTest2_3_4 {

}
