package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanAvg2LargerThanMeanTest.class,
		BAMeanDistanceToAvgBelowTest.class })
public class BAMeanTest2_2_4 {

}
