package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanAvg1SmallerThanMean.class,
		BAMeanAvg2LargerThanMeanTest.class, BAMeanDistanceToAvgAbvTest.class,
		BAMeanDistanceToAvgBelowTest.class })
public class BAMeanTest4_5 {

}
