package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanAvg1SmallerThanMean.class, BAMeanDistanceToMeanTest.class })
public class BAMeanTest2_1_5 {

}
