package bamean.tests;

import wv.TestUtility;
import bamean.programs.BAMeanInterface;
import bamean.programs.proper.BAMean;

public class BAMeanTestUtility implements TestUtility {
	
	public static boolean testEqualsDistances(double mean, double[] input) {
		double distanceabv = 0.0;
		double distancebel = 0.0;
		for (double i : input) {
			if (i < mean) {
				distancebel += Math.abs(mean - i);
			}
			else if (i > mean) {
				distanceabv += Math.abs(mean - i);
			}
		}
	
		return (distanceabv == distancebel);
	}
	
	public static Class<?> classToTest = BAMean.class;	
	

	public static BAMeanInterface getObjectOfClassToTest() {
		try {
			return (BAMeanInterface) classToTest.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public void setClassToTest(Class<?> newClass) {
		classToTest = newClass;
		
	}
		

}
