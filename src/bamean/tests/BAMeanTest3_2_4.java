package bamean.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BAMeanAvg1SmallerThanMean.class,
		BAMeanDistanceToAvgAbvTest.class, BAMeanDistanceToMeanTest.class })
public class BAMeanTest3_2_4 {

}
