package bamean.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BAMeanDistanceToAvgAbvTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		double[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		double[] result = BAMeanTestUtility.getObjectOfClassToTest().bamean(input);
		double mean = result[0];
		int i = 0;
		for (double x : input)
			if (x < mean)
				i++;
		
		double[] higher = new double[i];
		i = 0;
		for (double x : input)
			if (x > mean)
				higher[i++] = x;
		assertTrue(BAMeanTestUtility.testEqualsDistances(result[2], higher));
	}

}
