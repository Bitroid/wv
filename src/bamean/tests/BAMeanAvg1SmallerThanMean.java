package bamean.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BAMeanAvg1SmallerThanMean {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		double[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		double[] result = BAMeanTestUtility.getObjectOfClassToTest().bamean(input);
		assertTrue(result[0] >= result[1]);
	}

}
