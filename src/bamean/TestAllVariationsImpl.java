package bamean;

import java.util.*;

import bamean.programs.bad.*;
import bamean.tests.*;

import wv.TestAllVariations;

public class TestAllVariationsImpl {
	
	

	public static void main(String args[])
	{
		List<Class<?>> classes;
		List<Class<?>> programClasses;
		classes = new ArrayList<Class<?>>();
		classes.add(AllTests.class);
		classes.add(BAMeanAvg1SmallerThanMean.class);
		classes.add(BAMeanAvg2LargerThanMeanTest.class);
		classes.add(BAMeanDistanceToAvgAbvTest.class);
		classes.add(BAMeanDistanceToAvgBelowTest.class);
		classes.add(BAMeanDistanceToMeanTest.class);
		classes.add(BAMeanTest2_1_2.class);
		classes.add(BAMeanTest2_1_3.class);
		classes.add(BAMeanTest2_1_4.class);
		classes.add(BAMeanTest2_1_5.class);
		classes.add(BAMeanTest2_2_3.class);
		classes.add(BAMeanTest2_2_4.class);
		classes.add(BAMeanTest2_2_5.class);
		classes.add(BAMeanTest2_3_4.class);
		classes.add(BAMeanTest2_3_5.class);
		classes.add(BAMeanTest2_4_5.class);
		classes.add(BAMeanTest3_1_2.class);
		classes.add(BAMeanTest3_1_3.class);
		classes.add(BAMeanTest3_1_4.class);
		classes.add(BAMeanTest3_1_5.class);
		classes.add(BAMeanTest3_2_3.class);
		classes.add(BAMeanTest3_2_4.class);
		classes.add(BAMeanTest3_2_5.class);
		classes.add(BAMeanTest3_3_4.class);
		classes.add(BAMeanTest3_3_5.class);
		classes.add(BAMeanTest3_4_5.class);
		classes.add(BAMeanTest4_1.class);
		classes.add(BAMeanTest4_2.class);
		classes.add(BAMeanTest4_3.class);
		classes.add(BAMeanTest4_4.class);
		classes.add(BAMeanTest4_5.class);
		programClasses = new ArrayList<Class<?>>();
		programClasses.add(BAMean1_1.class);
		programClasses.add(BAMean1_2.class);
		programClasses.add(BAMean1_5.class);
		programClasses.add(BAMean2_1_2.class);
		programClasses.add(BAMean2_1_4.class);
		programClasses.add(BAMean2_1_5.class);
		programClasses.add(BAMean2_2_3.class);
		programClasses.add(BAMean2_2_5.class);
		programClasses.add(BAMean3_1_4.class);
		programClasses.add(BAMean3_2_3.class);
		programClasses.add(BAMean3_3_4.class);
		programClasses.add(BAMean3_3_5.class);
		programClasses.add(BAMean3_4_5.class);
		programClasses.add(BAMeanAvg1NotSmaller.class);
		programClasses.add(BAMeanAvg2NotLarger.class);
		programClasses.add(BAMeanDistanceToAvg1Wrong.class);
		programClasses.add(BAMeanDistanceToAvg2Wrong.class);
		programClasses.add(BAMeanDistanceToMeanWrong.class);
		
		TestAllVariations.testAllVariations(new BAMeanTestUtility(), classes, programClasses);
		
	}

}
