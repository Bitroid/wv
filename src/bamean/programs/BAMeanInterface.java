package bamean.programs;

public interface BAMeanInterface {
	public double[] bamean(double[] input);
}
