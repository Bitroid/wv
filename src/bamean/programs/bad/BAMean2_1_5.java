package bamean.programs.bad;

import bamean.programs.BAMeanInterface;

public class BAMean2_1_5 implements BAMeanInterface {
	public double[] bamean(double[] input) {
		double sum = 0.0;
		int below = 0;
		int above = 0;
		double sumabv = 0.0;
		double sumbel = 0.0;
		double avgbel = 0.0;
		double avgabv = 0.0;
		
		for (double x : input)
			sum += x;
		double mean = sum / input.length;
		
		for (double x : input) {
			if (x < mean) {
				sumbel += x;
				below++;
			}
		}
		
		if (below != 0)
			avgbel = sumbel / below - 1;
			
		return new double[] {mean, mean-1, avgbel};
		
	}
}
