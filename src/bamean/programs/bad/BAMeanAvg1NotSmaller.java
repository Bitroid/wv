package bamean.programs.bad;

import bamean.programs.BAMeanInterface;

public class BAMeanAvg1NotSmaller implements BAMeanInterface {
	
	public double[] bamean(double[] input) {
		double sum = 0.0;
		int below = 0;
		int above = 0;
		double sumabv = 0.0;
		double sumbel = 0.0;
		double avgbel = 0.0;
		double avgabv = 0.0;
		
		for (double x : input)
			sum += x;
		double mean = sum / input.length;
		
		for (double x : input) {
			if (x < mean) {
				sumbel += x;
				below++;
			}
			else if (x > mean) {
				sumabv += x;
				above++;
			}
		}
		
		if (below != 0)
			avgbel = sumbel / below;
		
		if (above != 0)
			avgabv = sumabv / above;
			
		return new double[] {mean, avgabv, avgabv};
		
	}

}
