package bamean.programs.proper;

import bamean.programs.BAMeanInterface;

public class BAMean implements BAMeanInterface {

	public double[] bamean(double[] input) {
		double sum = 0.0;
		int below = 0;
		int above = 0;
		double sumabv = 0.0;
		double sumbel = 0.0;
		double avgbel = 0.0;
		double avgabv = 0.0;
		
		for (double x : input)
			sum += x;
		double mean = sum / input.length;
		
		for (double x : input) {
			if (x < mean) {
				sumbel += x;
				below++;
			}
			else if (x > mean) {
				sumabv += x;
				above++;
			}
		}
		
		if (below != 0)
			avgbel = sumbel / below;
		
		if (above != 0)
			avgabv = sumabv / above;
			
		return new double[] {mean, avgbel, avgabv};
		
	}
	
	// Mean
	
	// Sum of distance to smaller numbers = sum of distance to larger numbers
	// Avg 1 is lower than Avg2
	// Avg 1 is lower than Mean
	// Avg 2 is higher than Mean

}
