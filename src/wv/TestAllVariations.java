package wv;

import java.util.*;

import org.junit.runner.*;

/**
 * A class to test how many faulty programs are caught by each testsuite.
 * 
 * @author Anna-Elisabeth Schnell, Arne Van der Stappen, Stefanie Verhulst
 */
public class TestAllVariations {
	
	/**
	 * Test how many faulty programs each testsuite catches.
	 * The number of caught programs will be printed to the default output stream
	 * in the order the tests are in the list.
	 * 
	 * @param	testUtility
	 * 			The testutilty from which the testsuites get an instance of
	 * 			the object to test.
	 * @param	testClasses
	 * 			The testsuites to evaluate.
	 * @param	programClasses
	 * 			The faulty programs.
	 */
	public static void testAllVariations(TestUtility testUtility, List<Class<?>> testClasses, List<Class<?>> programClasses) {
		JUnitCore junitCore = new JUnitCore();
		for (Class<?> testClass : testClasses) {
			int caught = 0;
			for (Class<?> programClass : programClasses) {
				testUtility.setClassToTest(programClass);
				Result result = junitCore.run(testClass);
				if (result.getFailureCount() > 0) {
						caught++;
				}
			}
			System.out.println(testClass + "\t" + caught);
		}
		
	}

}
