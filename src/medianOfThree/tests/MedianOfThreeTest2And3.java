package medianOfThree.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({MedianOfThreeTestProperty2.class, MedianOfThreeTestProperty3.class})
public class MedianOfThreeTest2And3 {
	
		// Properties a correct medianOfThree implementation SHOULD have:
		// 1: Output must = at least 1 of 3 input ints
		// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
		// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	
}
