package medianOfThree.tests;

import static org.junit.Assert.*;

import org.junit.Test;

public class MedianOfThreeOptimalTest {
	
		// Properties a correct medianOfThree implementation SHOULD have:
		// 1: Output must = at least 1 of 3 input ints
		// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
		// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	
	@Test
	public void testOptimal() {
		// Three equal numbers as input.
		int input1 = 3;
		int input2 = 3;
		int input3 = 3;
		int output = MedianOfThreeTestUtility.getObjectOfClassToTest().medianOfThree(input1, input2, input3);
		assertTrue(output == 3);
		// Three different numbers as input.
		input1 = 1;
		input2 = 2;
		input3 = 3;
		output = MedianOfThreeTestUtility.getObjectOfClassToTest().medianOfThree(input1, input2, input3);
		assertTrue(output == 2);
		// input1 and input2 equal but input3 different
		input1 = 1;
		input2 = 1;
		input3 = 3;
		output = MedianOfThreeTestUtility.getObjectOfClassToTest().medianOfThree(input1, input2, input3);
		assertTrue(output == 1);
		// input2 and input3 equal but input1 different
		input1 = 1;
		input2 = 3;
		input3 = 3;
		output = MedianOfThreeTestUtility.getObjectOfClassToTest().medianOfThree(input1, input2, input3);
		assertTrue(output == 3);
	}
}

