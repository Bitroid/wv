package medianOfThree.tests;

import wv.TestUtility;
import medianOfThree.programs.MedianOfThree;
import medianOfThree.programs.proper.MedianOfThreeCorrect2;

public class MedianOfThreeTestUtility implements TestUtility {
	
	public static Class<?> classToTest = MedianOfThreeCorrect2.class;	
	

	public static MedianOfThree getObjectOfClassToTest() {
		try {
			return (MedianOfThree) classToTest.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public void setClassToTest(Class<?> newClass) {
		classToTest = newClass;
		
	}
	
}
