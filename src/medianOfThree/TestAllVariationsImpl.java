package medianOfThree;

import java.util.*;

import medianOfThree.programs.bad.*;
import medianOfThree.tests.*;

import wv.TestAllVariations;

public class TestAllVariationsImpl {
	
	

	public static void main(String args[])
	{
		List<Class<?>> testClasses;
		List<Class<?>> programClasses;
		testClasses = new ArrayList<Class<?>>();
		testClasses.add(MedianOfThreeTestProperty1.class);
		testClasses.add(MedianOfThreeTestProperty2.class);
		testClasses.add(MedianOfThreeTestProperty3.class);
		testClasses.add(MedianOfThreeTest1And2.class);
		testClasses.add(MedianOfThreeTest1And3.class);
		testClasses.add(MedianOfThreeTest2And3.class);
		testClasses.add(MedianOfThreeTestAllProperties.class);
		//Other test classes:
		testClasses.add(MedianOfThreeTestProperty2Strongly.class);
		testClasses.add(MedianOfThreeTestProperty3Strongly.class);

		
		programClasses = new ArrayList<Class<?>>();
		//programClasses.add(MedianOfThreeCorrect.class);
		//programClasses.add(MedianOfThreeCorrect2.class);
		programClasses.add(MedianOfThreeBreakingProperty1.class);
		programClasses.add(MedianOfThreeBreakingProperty2.class);
		programClasses.add(MedianOfThreeBreakingProperty3.class);
		programClasses.add(MedianOfThreeBreakingProperty1And2.class);
		programClasses.add(MedianOfThreeBreakingProperty1And3.class);
		
		TestAllVariations.testAllVariations(new MedianOfThreeTestUtility(), testClasses, programClasses);
		
	}

}
