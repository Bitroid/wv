package medianOfThree.programs.proper;

import medianOfThree.programs.MedianOfThree;

public class MedianOfThreeCorrect2 implements MedianOfThree {
	
	// Properties a correct medianOfThree implementation SHOULD have:
		// 1: Output must = at least 1 of 3 input ints
		// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
		// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)

	public int medianOfThree(int input1, int input2, int input3) {
		if (input1 > input2) {
			if (input2 > input3) {
				return input2;
			} else if (input1 > input3) {
				return input3;
			} else {
				return input1;
			}
		} else {
			if (input1 > input3) {
				return input1;
			} else if (input2 > input3) {
				return input3;
			} else {
				return input2;
			}
		}
	}

}
