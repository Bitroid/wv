package medianOfThree.programs;

public interface MedianOfThree {
	
	public int medianOfThree(int input1, int input2, int input3);
	
	// Properties a correct medianOfThree implementation SHOULD have:
	// 1: Output must = at least 1 of 3 input ints
	// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)

}
