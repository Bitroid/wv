package medianOfThree.programs.bad;

import java.util.Random;

import medianOfThree.programs.MedianOfThree;

public class MedianOfThreeBreakingProperty1 implements MedianOfThree {
		
	// Properties a correct medianOfThree implementation SHOULD have:
	// 1: Output must = at least 1 of 3 input ints
	// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	
	public int medianOfThree(int input1, int input2, int input3) {
		Random random = new Random(1);
		if ((input2 <= input1) && (input1 <= input3)) {
			return input1 + (int)(random.nextInt(input3 - input1));
		}
		if ((input3 <= input1) && (input1 <= input2)) {
			return input1 + (int)(random.nextInt(input2 - input1));
		}
		if ((input1 <= input2) && (input2 <= input3)) {
			return input2 + (int)(random.nextInt(input3 - input2));
		}
		if ((input3 <= input2) && (input2 <= input1)) {
			return input2 + (int)(random.nextInt(input1 - input2));
		}
		if ((input1 <= input3) && (input3 <= input2)) {
			return input3 + (int)(random.nextInt(input2 - input3));
		}
		if ((input2 <= input3) && (input3 <= input1)) {
			return input3 + (int)(random.nextInt(input1 - input3));
		}
		throw new IllegalArgumentException("Invalid input.");
 	}
	
}
