package medianOfThree.programs.bad;

import medianOfThree.programs.MedianOfThree;

public class MedianOfThreeCorrect implements MedianOfThree {

	public int medianOfThree(int input1, int input2, int input3) {
		if (((input2 <= input1) && (input1 <= input3)) || ((input3 <= input1) && (input1 <= input2))) return input1;
		if (((input1 <= input2) && (input2 <= input3)) || ((input3 <= input2) && (input2 <= input1))) return input2;
		if (((input1 <= input3) && (input3 <= input2)) || ((input2 <= input3) && (input3 <= input1))) return input3;
		throw new IllegalArgumentException("Invalid input.");
 	}
		
	// Properties a correct medianOfThree implementation SHOULD have:
	// 1: Output must = at least 1 of 3 input ints
	// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)

}
