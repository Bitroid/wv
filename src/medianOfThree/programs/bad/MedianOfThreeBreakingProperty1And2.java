package medianOfThree.programs.bad;

import medianOfThree.programs.MedianOfThree;

public class MedianOfThreeBreakingProperty1And2 implements MedianOfThree {
	
	// Properties a correct medianOfThree implementation SHOULD have:
	// 1: Output must = at least 1 of 3 input ints
	// 2: Output must <= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	// 3: Output must >= at least 1 of 3 input ints (exactly 1 if there's no doubles)
	
	public int medianOfThree(int input1, int input2, int input3) {
		if (((input3 <= input2) && (input2 <= input1)) || ((input2 <= input3) && (input3 <= input1))) {
			return input1 + 1;
		}
		if (((input3 <= input1) && (input1 <= input2)) || ((input1 <= input3) && (input3 <= input2))) {
			return input2 + 1;
		}
		if (((input2 <= input1) && (input1 <= input3)) || ((input1 <= input2) && (input2 <= input3))) {
			return input3 + 1;
		}
		throw new IllegalArgumentException("Invalid input.");	
	}

}
