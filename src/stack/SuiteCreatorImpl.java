package stack;
import java.util.*;

import stack.tests.*;

import wv.SuiteCreator;

public class SuiteCreatorImpl {

	public static String PACKAGEDIR = "src/stack/tests/suites/";
	public static String PACKAGE = "stack.tests.suites";
	public static String CLASSPREFIX = "StackTest";

	public static void main(String args[])
	{
		
		try{
			List<Class<?>> classes = new ArrayList<Class<?>>();
			
			classes.add(StackTestProperty2a.class);
			classes.add(StackTestProperty2b.class);
			classes.add(StackTestProperty3a.class);
			classes.add(StackTestProperty3b.class);
			classes.add(StackTestProperty4a.class);
			classes.add(StackTestProperty4b.class);
			classes.add(StackTestProperty5.class);
			classes.add(StackTestProperty7.class);
			
			Map<Class<?>, List<Class<?>>> redundancies = new HashMap<Class<?>, List<Class<?>>>();
			List<Class<?>> list = new ArrayList<Class<?>>();
			list.add(StackTestProperty3b.class);
			redundancies.put(StackTestProperty3a.class, list);
			
			list = new ArrayList<Class<?>>();
			list.add(StackTestProperty4b.class);
			redundancies.put(StackTestProperty4a.class, list);
			
			SuiteCreator.createSuites(PACKAGE, CLASSPREFIX, classes, redundancies);

		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
}
