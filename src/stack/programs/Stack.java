package stack.programs;
public interface Stack<Item> extends Iterable<Item> {
	
	//Properties a Stack implementation needs to have to be correct:
	// 1: LIFO 
	// *2.1: popping the Stack with pop() returns the element last added
	// *2.2: popping the Stack with pop() removes the element last added
	// *3.1: looking at the Stack with peek() returns the element last added
	// *3.2: looking at the Stack with peek() does not remove the element last added
	// *4.1: pushing onto the Stack with push(item) changes the top element into the item
	// *4.2: pushing onto the Stack with push(item) moves the old last pushed item down the stack one.
	// *5: size() must return the amount of elements in the Stack
	// 6: check() must make sure all internal invariants are still there:
	// 6.1: an empty stack has no first element
	// 6.2: a stack of size one has one element
	// 6.3: a stack larger than size one has more than one element
	// *7: isEmpty() is only true if the stack size is also 0
	//
	// *: Tested properties

    /**
     * Is the stack empty?
     */
    public boolean isEmpty();

   /**
     * Return the number of items in the stack.
     */
    public int size();

   /**
     * Add the item to the stack.
     */
    public void push(Item item);

   /**
     * Delete and return the item most recently added to the stack.
     * @throws java.util.NoSuchElementException if stack is empty.
     */
    public Item pop();


   /**
     * Return the item most recently added to the stack.
     * @throws java.util.NoSuchElementException if stack is empty.
     */
    public Item peek();

   /**
     * Return string representation.
     */
    public String toString();
}
