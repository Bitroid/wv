package stack.tests;

import stack.programs.Stack;
import stack.programs.proper.StackCorrect;
import wv.TestUtility;

public class StackTestUtility implements TestUtility {
	
	public static Class<?> classToTest = StackCorrect.class;	
	
	@SuppressWarnings("unchecked")
	public static Stack<Integer> getObjectOfClassToTest() {
		try {
			return (Stack<Integer>) classToTest.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public void setClassToTest(Class<?> newClass) {
		classToTest = newClass;
		
	}
	
}
