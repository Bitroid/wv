package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty2a.class,
stack.tests.StackTestProperty2b.class })
public class StackTest2_1_2 {

}
