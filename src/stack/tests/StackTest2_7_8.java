package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty5.class,
stack.tests.StackTestProperty7.class })
public class StackTest2_7_8 {

}
