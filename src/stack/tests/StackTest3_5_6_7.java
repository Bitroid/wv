package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty4a.class,
stack.tests.StackTestProperty4b.class,
stack.tests.StackTestProperty5.class })
public class StackTest3_5_6_7 {

}
