package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty2b.class,
stack.tests.StackTestProperty3a.class,
stack.tests.StackTestProperty3b.class,
stack.tests.StackTestProperty5.class })
public class StackTest4_2_3_4_7 {

}
