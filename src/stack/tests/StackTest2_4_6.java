package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty3b.class,
stack.tests.StackTestProperty4b.class })
public class StackTest2_4_6 {

}
