package stack.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import stack.programs.Stack;

public class StackTestProperty3a {
	//Properties a Stack implementation needs to have to be correct:
			// 1: LIFO 
			// 2.1: popping the Stack with pop() returns the element last added
			// 2.2: popping the Stack with pop() removes the element last added
			// 3.1: looking at the Stack with peek() returns the element last added
			// 3.2: looking at the Stack with peek() does not remove the element last added
			// 4.1: pushing onto the Stack with push(item) changes the top element into the item
			// 4.2: pushing onto the Stack with push(item) moves the old last pushed item down the stack one.
			// 5: size() must return the amount of elements in the Stack
			// 6: check() must make sure all internal invariants are still there:
			// 6.1: an empty stack has no first element
			// 6.2: a stack of size one has one element
			// 6.3: a stack larger than size one has more than one element
			// 7: isEmpty() is only true if the stack size is also 0
	
	@Test
	public void checkPop() {
		Stack<Integer> stack = StackTestUtility.getObjectOfClassToTest();
		int pushedObject = 1;
		stack.push(pushedObject);
		assertTrue(stack.peek().equals(pushedObject));
	}
}
