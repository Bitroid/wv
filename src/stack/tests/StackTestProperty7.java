package stack.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import stack.programs.Stack;

public class StackTestProperty7 {
	// Properties a Stack implementation needs to have to be correct:
	// 1: LIFO 
	// 2: popping the Stack with pop() removes and returns the element last added
	// 3: looking at the Stack with peek() only returns the element last added without removing it
	// 4: pushing onto the Stack with push(item) changes the last pushed element into the item,
	//	  and moves the old last pushed item back.
	// 5: size() must return the amount of elements in the Stack
	// 6: check() must make sure all internal invariants are still there:
	// 6.1: an empty stack has no first element
	// 6.2: a stack of size one has one element
	// 6.3: a stack larger than size one has more than one element
	// 7: isEmpty() is only true if the stack size is also 0
	
	@Test
	public void checkIsEmpty() {
		Stack<Integer> stack = StackTestUtility.getObjectOfClassToTest();
		assertTrue(stack.isEmpty() && stack.size() == 0);
		int oldPushedObject = 1;
		int newPushedObject = 2;
		stack.push(oldPushedObject);
		stack.push(newPushedObject);
		assertTrue(stack.size() == 2 && !stack.isEmpty());
	}
}