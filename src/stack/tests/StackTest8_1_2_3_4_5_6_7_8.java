package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty2a.class,
stack.tests.StackTestProperty2b.class,
stack.tests.StackTestProperty3a.class,
stack.tests.StackTestProperty3b.class,
stack.tests.StackTestProperty4a.class,
stack.tests.StackTestProperty4b.class,
stack.tests.StackTestProperty5.class,
stack.tests.StackTestProperty7.class })
public class StackTest8_1_2_3_4_5_6_7_8 {

}
