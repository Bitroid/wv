package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
stack.tests.StackTestProperty2b.class,
stack.tests.StackTestProperty5.class,
stack.tests.StackTestProperty7.class })
public class StackTest3_2_7_8 {

}
