package calculate;

public class Calculate {
	
	public static int sum(int[] input) {
		int sum = 0;
		for (int i : input)
			sum += i;
		return sum;
	}
	
	public static int multiply(int[] input) {
		int product = 1;
		for (int i : input)
			product *= i;
		return product;
	}
	
	public static double avg(int[] input) {
		if (input.length == 0)
			return 0;
		double sum = 0;
		for (int i : input)
			sum += i;
		return sum / input.length;
	}

}
